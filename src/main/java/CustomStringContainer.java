import java.util.Arrays;
import java.util.Iterator;

public class CustomStringContainer implements Iterable<String>, Cloneable {

  private String[] arrayOfString;
  int sizeOfArray = 16;
  String tail;
  int lastIndex = 0;
  String head;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof CustomStringContainer)) {
      return false;
    }
    CustomStringContainer that = (CustomStringContainer) o;
    return Arrays.equals(arrayOfString, that.arrayOfString);
  }

  public CustomStringContainer() {
    arrayOfString = new String[sizeOfArray];
  }

  @Override
  protected Object clone() throws CloneNotSupportedException {
    return super.clone();
  }

  protected Object clone(CustomStringContainer that) throws CloneNotSupportedException {
    if (that == null) {
      return super.clone();
    } else {
      this.head = that.head;
      this.arrayOfString = that.arrayOfString;
      this.sizeOfArray = that.sizeOfArray;
      this.tail = that.tail;
      this.lastIndex = that.lastIndex;
      return this;
    }
  }

  @Override
  public int hashCode() {
    return Arrays.hashCode(arrayOfString);
  }

  public CustomStringContainer(String[] arrayOfString, int sizeOfArray, String tail,
      String head) {
    this.arrayOfString = arrayOfString;
    this.sizeOfArray = sizeOfArray;
    this.tail = tail;
    this.head = head;
  }

  public CustomStringContainer(String[] arrayOfString, int sizeOfArray, String tail, int lastIndex,
      String head) {
    this.arrayOfString = arrayOfString;
    this.sizeOfArray = sizeOfArray;
    this.tail = tail;
    this.lastIndex = lastIndex;
    this.head = head;
  }

  private CustomStringContainer grow() {
    String[] temp = new String[(int) ((this.arrayOfString.length) * 1.5)];
    for (int i = 0; i <= this.arrayOfString.length - 1; i++) {
      if (this.arrayOfString[i] == "") {

      } else {
        temp[i] = this.arrayOfString[i];
      }
    }
    this.arrayOfString = temp;
    this.sizeOfArray = (int) ((this.arrayOfString.length) * 1.5);
    this.tail = temp[temp.length-1];
    return this;
  }

  public CustomStringContainer(int sizeOfArray) {
    arrayOfString = new String[sizeOfArray];
    this.sizeOfArray = sizeOfArray;

  }

  public boolean add(String o) {
    if ((lastIndex+1) * 1.5 >= sizeOfArray) {
      this.grow();
    }
    int i = 0;
    try {
      while (!(arrayOfString[i].equals(null))) {
        i++;
      }
    } catch (NullPointerException e) {
      lastIndex = i;
      arrayOfString[lastIndex] = o;
    }
    return true;
  }

  public String get(int index) {
    try {

    if (!(arrayOfString[index].equals(null)))
      return arrayOfString[index];

  }
  catch (NullPointerException e){
    return "";
  }
  return "";
  }

  public Iterator<String> iterator() {

    return null;
  }
}
